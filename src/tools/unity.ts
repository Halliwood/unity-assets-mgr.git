import fs from 'fs-extra'

export async function readGUID (metaFile: string): Promise<string | undefined> {
    const fcontent = await fs.readFile(metaFile, 'utf-8')
    const mr = fcontent.match(/^guid: (\w{32})$/m)
    return mr?.[1]
}
