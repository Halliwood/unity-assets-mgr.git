import { program } from 'commander'

import { FileUsing } from './FileUsing.js'

declare type Action = 'uselessimgs' | 'refer'

interface CmdOption {
    action: Action
    projectRoot: string
    file: string
}

const rmQuotes = (val: string): string => {
    const rst = val.match(/(['"])(.+)\1/)
    if (rst != null) return rst[2]
    return val
}

program
    .option('-a, --action <string>', '[MUST] What kind of action do you want to take.', rmQuotes)
    .option('-p, --project-root <string>', '[MUST] The project root.', rmQuotes)
    .option('-f, --file [string]', 'The file path.', rmQuotes)
    .parse(process.argv)

const opts = program.opts<CmdOption>()
console.log(`cmd options: ${JSON.stringify(opts)}`)

if (opts.action === 'uselessimgs') {
    await FileUsing.Instance.collectUselessImgs(opts.projectRoot)
} else if (opts.action === 'refer') {
    await FileUsing.Instance.getRefer(opts.projectRoot, opts.file)
}
console.log('finished')
