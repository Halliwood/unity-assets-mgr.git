# unity-assets-mgr

#### 介绍
Unity assets manager, handling issues like assets using statics etc.


#### 安装教程

```
npm i unity-assets-mgr -g
```

#### 使用说明

1.  Search useless atlas images
```
unity-assets-mgr -a uselessimgs -p G:/muweb/trunk/project
```

2. Get atlas image references
```
unity-assets-mgr -a refer -p G:/muweb/trunk/project -f Assets/AssetSources/ui/atlas/specialize/loading/dljdt.png
```