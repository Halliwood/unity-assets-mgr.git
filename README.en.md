# unity-assets-mgr

#### Description
Unity assets manager, handling issues like assets using statics etc.

#### Installation

```
npm i unity-assets-mgr -g
```

#### Instructions

1.  Search useless atlas images
```
unity-assets-mgr -a uselessimgs -p G:/muweb/trunk/project
```

2. Get atlas image references
```
unity-assets-mgr -a refer -p G:/muweb/trunk/project -f Assets/AssetSources/ui/atlas/specialize/loading/dljdt.png
```
